package com.vimtura.vimphone.header;

import org.linphone.core.LinphoneCall;
import org.linphone.core.LinphoneCallParams;

import com.vimtura.vimphone.R;
import com.vimtura.vimphone.ui.MarqueeTextView;
import com.vimtura.vimphone.util.DateUtil;
import com.vimtura.vimphone.util.TimeFormatStyle;

import android.app.Activity;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TableRow;
import android.widget.TextView;

/**
 * VimturaQueue
 * Queue information derrived from SIP headers.
 * 
 * @author david
 *
 */
public class VimturaQueue {
	
	public final static String HEADER_QUEUE_NAME = "X-Vimtura-Queue-Name";
	public final static String HEADER_QUEUE_TOTAL = "X-Vimtura-Queue-Total";
	public final static String HEADER_QUEUE_ABANDONED = "X-Vimtura-Queue-Abandoned";
	public final static String HEADER_QUEUE_AVG_TIME = "X-Vimtura-Queue-Avg-Time";	
	public final static String HEADER_QUEUE_CUR_TIME = "X-Vimtura-Queue-Cur-Time";		
	public final static String HEADER_QUEUE_CUR_ORIG_POS = "X-Vimtura-Queue-Cur-Orig-Pos";		
	
	protected VimturaHeader header;
	protected String queueName;
	protected Integer queueSize;
	protected Integer queueAbandoned;
	protected Integer queueAvgHoldTime;
	protected Integer callerHoldTime;
	protected Integer callerOriginalPosition;
	
	public VimturaQueue(VimturaHeader header) {
		this.header = header;
		this.parse();
	}
	
	protected void parse() {
		this.queueName = header.getHeader(HEADER_QUEUE_NAME);
		
		String callerOrigPos = header.getHeader(HEADER_QUEUE_CUR_ORIG_POS);
		if (callerOrigPos != null) {
			this.callerOriginalPosition = Integer.parseInt(callerOrigPos);
		}
		
		String queueSizeString = header.getHeader(HEADER_QUEUE_TOTAL);
		if (queueSizeString != null) {
			this.queueSize = Integer.parseInt(queueSizeString);
		}
		
		String queueAbandonedString = header.getHeader(HEADER_QUEUE_ABANDONED);
		if (queueAbandonedString != null) {
			this.queueAbandoned = Integer.parseInt(queueAbandonedString);
		}		
		
		String queueAvgHoldTimeString = header.getHeader(HEADER_QUEUE_AVG_TIME);
		if (queueAvgHoldTimeString != null) {
			this.queueAvgHoldTime = Integer.parseInt(queueAvgHoldTimeString);
		}
		
		String callerHoldTimeString = header.getHeader(HEADER_QUEUE_CUR_TIME);
		if (callerHoldTimeString != null) {
			this.callerHoldTime = Integer.parseInt(callerHoldTimeString);
		}
	}	
	
	protected String getQueueSizeDesc() {
		if (this.queueSize != null) {
			String desc = this.queueSize.toString();
			if (this.queueAvgHoldTime != null) {
				desc += " (avg. " + DateUtil.formatSeconds(this.queueAvgHoldTime, TimeFormatStyle.SHORT) +")";
			}
			return desc;
		}
		return null;
	}
	
	protected String getCallerWaitDesc() {
		if (this.callerHoldTime != null) {
			return DateUtil.formatSeconds(this.callerHoldTime, TimeFormatStyle.SHORT);
		}
		return null;
	}
	
	public void fillOutView(Activity activity, ViewGroup queueView) {
		TableRow nameRow = (TableRow) activity.findViewById(R.id.queue_name);
		if (nameRow != null) {
			nameRow.setVisibility(View.GONE);
			if (this.queueName != null && this.fillOutDetailTableRowValue(nameRow, this.queueName)) {
				nameRow.setVisibility(View.VISIBLE);
			}
		}
		
		TableRow sizeRow = (TableRow) activity.findViewById(R.id.queue_size);
		if (sizeRow != null) {
			sizeRow.setVisibility(View.GONE);
			String queueSizeDesc = this.getQueueSizeDesc();
			if (queueSizeDesc != null && this.fillOutDetailTableRowValue(sizeRow, queueSizeDesc)) {
				sizeRow.setVisibility(View.VISIBLE);
			}
		}		
		
		TableRow callerWaitRow = (TableRow) activity.findViewById(R.id.queue_caller_wait);	
		if (callerWaitRow != null) {
			callerWaitRow.setVisibility(View.GONE);
			String callerWaitDesc = this.getCallerWaitDesc();
			if (callerWaitDesc != null && this.fillOutDetailTableRowValue(callerWaitRow, callerWaitDesc)) {
				callerWaitRow.setVisibility(View.VISIBLE);
			}
		}		
		
		TableRow callerOrigPosRow = (TableRow) activity.findViewById(R.id.queue_caller_orig_pos);
		if (callerOrigPosRow != null) {
			callerOrigPosRow.setVisibility(View.GONE);
			if (this.callerOriginalPosition != null && this.fillOutDetailTableRowValue(callerOrigPosRow, this.callerOriginalPosition.toString())) {
				callerOrigPosRow.setVisibility(View.VISIBLE);
			}
		}
		
		TableRow abandonedRow = (TableRow) activity.findViewById(R.id.queue_abandoned);
		if (abandonedRow != null) {
			abandonedRow.setVisibility(View.GONE);
			if (this.queueAbandoned != null && this.fillOutDetailTableRowValue(abandonedRow, this.queueAbandoned.toString())) {
				abandonedRow.setVisibility(View.VISIBLE);
			}
		}		
	}
	
	protected boolean fillOutDetailTableRowValue(TableRow row, String value) {
		View valueField = row.getChildAt(1);
		if (valueField != null) {
			if (valueField instanceof TextView) {
				((TextView) valueField).setText(value);
				
				return true;
			}
		}
		
		return false;
	}
	
	public String toString() {
		return (new StringBuilder())
			.append("{ VimturaQueue [")
			.append(" queueName: ").append(queueName)		
			.append("] }")
			.toString();
	}		
	
}
